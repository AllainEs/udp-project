<?php
require_once('bdd.php');

$search = filter_input(INPUT_POST, "search");

if ($search == NULL)
{
  include("index.php");
  exit();
} else
{
  $query = "Select * from events where events.title like '%" . $search . "%'";
  $stmt = $bdd->prepare($query);
  $stmt->execute();
  $results = $stmt->fetchAll();
  $stmt->closeCursor();
}
?>

<html>
  <head>
    <title>Search Results</title>
  </head>

  <body>
      <?php
      foreach ($results as $result):
        echo $result['title'];
        echo $result['venue'];
        echo $result['lecturer'];
        echo $result['start'];
        echo $result['end'];
      endforeach;
      ?>
  </body>
</html>