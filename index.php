<?php
require_once('bdd.php');


$sql = "SELECT id, title,venue,lecturer,start, categories, end, color FROM events where categories = 'Academic'";

$req = $bdd->prepare($sql);
$req->execute();

$events = $req->fetchAll();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Event I/O</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/additions.css" rel="stylesheet">
    <link href='css/fullcalendar.css' rel='stylesheet' />
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <style>
      body
      {
          padding-top: 70px;
          background-image: linear-gradient(to top, #fdcbf1 0%, #fdcbf1 1%, #e6dee9 100%);
          background-size:cover;
          max-width: 100%;
          max-height:100%;
      }
      .background
      {
          background-color:#2c3e50;
      }
      #calendar 
      {
          max-width: 100%;
          max-height:100%;
      }
      .col-centered
      {
          float: none;
          -webkit-box-shadow: 0px 6px 38px 3px rgba(0,0,0,0.75);
          -moz-box-shadow: 0px 6px 38px 3px rgba(0,0,0,0.75);
          box-shadow: 0px 6px 38px 3px rgba(0,0,0,0.75);
          margin: 0 auto;
          max-width: 100%;
          max-height:100%;
          background-image: linear-gradient(to top, #a18cd1 0%, #fbc2eb 100%);
      }
      .table
      {
          -webkit-box-shadow: 0px 6px 38px 3px rgba(0,0,0,0.75);
          -moz-box-shadow: 0px 6px 38px 3px rgba(0,0,0,0.75);
          box-shadow: 0px 6px 38px 3px rgba(0,0,0,0.75);
      }
    </style>
  </head>
  <body>
    <div class="calendar-picker"><center> <a class="btn btn-primary" href="non-academic.php" role="button">GO TO NON ACADEMIC CALENDAR</a></center></div>
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">HOME</a>
        </div>       
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav">
            <li>
              <a href="login.php">LOG IN</a>
            </li>
            <li>
              <a href="#table">Missed Events</a>
            </li>           
            <li>

            </li>
          </ul>
        </div>          
      </div>       
    </nav>
    <div class="container">

      <div class="row">
        <div class="col-lg-12 text-center">
          <svg xmlns="http://www.w3.org/2000/svg" style="display:none">
          <symbol xmlns="http://www.w3.org/2000/svg" id="sbx-icon-search-22" viewBox="0 0 40 40">
            <path d="M24.382 25.485c-1.704 1.413-3.898 2.263-6.292 2.263-5.42 0-9.814-4.36-9.814-9.736 0-5.377 4.394-9.736 9.814-9.736s9.815 4.36 9.815 9.736c0 2.126-.687 4.093-1.853 5.694l5.672 5.627-1.73 1.718-5.612-5.565zM20 40c11.046 0 20-8.954 20-20S31.046 0 20 0 0 8.954 0 20s8.954 20 20 20zm-1.91-14.686c4.065 0 7.36-3.27 7.36-7.302 0-4.033-3.295-7.302-7.36-7.302s-7.36 3.27-7.36 7.302c0 4.033 3.295 7.302 7.36 7.302z"
                  fill-rule="evenodd" />
          </symbol>
          <symbol xmlns="http://www.w3.org/2000/svg" id="sbx-icon-clear-4" viewBox="0 0 20 20">
            <path d="M11.664 9.877l4.485 4.485-1.542 1.54-4.485-4.485-4.485 4.485-1.54-1.54 4.485-4.485-4.485-4.485 1.54-1.54 4.485 4.484 4.485-4.485 1.54 1.542-4.484 4.485zM10 20c5.523 0 10-4.477 10-10S15.523 0 10 0 0 4.477 0 10s4.477 10 10 10z" fill-rule="evenodd"
                  />
          </symbol>
          </svg>

          <form method="POST" class="searchbox sbx-custom" action="search.php">
            <div role="search" class="sbx-custom__wrapper">
              <input type="text" name="search" id="search" placeholder="Search" class="sbx-custom__input">
              <button type="submit" title="Submit your search query." class="sbx-custom__submit">
                <svg role="img" aria-label="Search">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#sbx-icon-search-22"></use>
                </svg>
              </button>
              <button type="reset" title="Clear the search query." class="sbx-custom__reset">
                <svg role="img" aria-label="Reset">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#sbx-icon-clear-4"></use>
                </svg>
              </button>
            </div>
          </form>
          <script type="text/javascript">
             document.querySelector('.searchbox [type="reset"]').addEventListener('click', function () {
               this.parentNode.querySelector('input').focus();
             });
          </script>
        </div>

        <div id="calendar" class="col-centered">
        </div>
      </div>
      <div class="page-header">
        <h3>Occured/Missed Events</h3>
      </div>

      <div class="row">

        <div class="col-lg-12">

          <table class="table table-striped table-bordered" id="table">

            <thead>
              <tr>
                <th>Event Name</th>
                <th>View Details</th>
              </tr>
            </thead>

            <tbody>

              <?php
              require_once 'bdd.php';

              $query = "SELECT * FROM events where start <= NOW()";
              $stmt = $bdd->prepare($query);
              $stmt->execute();
              while ($row = $stmt->fetch(PDO::FETCH_ASSOC))
              {
                ?>
                <tr>
                  <td><?php echo $row['title']; ?></td>
                  <td>
                    <button data-toggle="modal" data-target="#view-modal" data-id="<?php echo $row['id']; ?>" id="getDetails" class="btn btn-sm btn-info"><i class="glyphicon glyphicon-eye-open"></i> View</button>
                  </td>
                </tr>
                <?php
              }
              ?>

            </tbody>
          </table>      

        </div>

      </div>




      <div id="view-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog"> 
          <div class="modal-content"> 

            <div class="modal-header"> 
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
              <h4 class="modal-title">
                <i class="glyphicon glyphicon-user"></i> Event Details
              </h4> 
            </div> 
            <div class="modal-body"> 

              <div id="modal-loader" style="display: none; text-align: center;">
                <img src="ajax-loader.gif">
              </div>

              <!-- content will be load here -->                          
              <div id="dynamic-content"></div>

            </div> 
            <div class="modal-footer"> 
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>  
            </div> 

          </div> 
        </div>
      </div><!-- /.modal -->    

    </div>


    <script src="js/jquery-1.12.4.min.js"></script>
    <script src="js/bootstrap.min.js"></script>


    <script>
             $(document).ready(function () {

               $(document).on('click', '#getDetails', function (e) {

                 e.preventDefault();

                 var uid = $(this).data('id');   // it will get id of clicked row

                 $('#dynamic-content').html(''); // leave it blank before ajax call
                 $('#modal-loader').show();      // load ajax loader

                 $.ajax({
                   url: 'occuredEvents.php',
                   type: 'POST',
                   data: 'id=' + uid,
                   dataType: 'html'
                 })
                   .done(function (data) {
                     console.log(data);
                     $('#dynamic-content').html('');
                     $('#dynamic-content').html(data); // load response 
                     $('#modal-loader').hide();		  // hide ajax loader	
                   })
                   .fail(function () {
                     $('#dynamic-content').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
                     $('#modal-loader').hide();
                   });

               });

             });

    </script>

  </div>
  <div class="modal fade" id="ModalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">

      </div>
    </div>
  </div>
  <div class="modal fade" id="ModalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <form class="form-horizontal" method="POST" action="editEventTitle.php">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Event Details</h4>
          </div>
          <div class="modal-body">

            <div class="form-group">
              <label for="title" class="col-sm-2 control-label">Title</label>
              <div class="col-sm-10">
                <input type="text" name="title" class="form-control" id="title" disabled>
              </div>
            </div>

            <div class="form-group">
              <label for="venue" class="col-sm-2 control-label">Venue</label>
              <div class="col-sm-10">
                <input type="text" name="venue" class="form-control" id="venue" disabled>
              </div>
            </div>

            <div class="form-group">
              <label for="lecturer" class="col-sm-2 control-label">Lecturer</label>
              <div class="col-sm-10">
                <input type="text" name="lecturer" class="form-control" id="lecturer" disabled>
              </div>
            </div>

            <div class="form-group">
              <label for="start" class="col-sm-2 control-label">Start</label>
              <div class="col-sm-10">
                <input type="text" name="start" class="form-control" id="start" disabled>
              </div>
            </div>

            <div class="form-group">
              <label for="end" class="col-sm-2 control-label">End</label>
              <div class="col-sm-10">
                <input type="text" name="end" class="form-control" id="end" disabled>
              </div>
            </div>

            <input type="hidden" name="id" class="form-control" id="id">


          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>     
<script src='js/moment.min.js'></script>
<script src='js/fullcalendar.min.js'></script>
<script>
             $(document).ready(function () {
               $('#calendar').fullCalendar({
                 header: {
                   left: 'prev,next today',
                   center: 'title',
                   right: 'month,basicWeek,basicDay'
                 },
                 height: $(window).height() * 0.83,
                 defaultView: 'basicWeek',
                 defaultDate: new Date(),
                 editable: true,
                 eventLimit: true,
                 selectable: true,
                 selectHelper: true,

                 eventRender: function (event, element) {
                   element.bind('dblclick', function () {
                     $('#ModalEdit #id').val(event.id);
                     $('#ModalEdit #title').val(event.title);
                     $('#ModalEdit #venue').val(event.venue);
                     $('#ModalEdit #lecturer').val(event.lecturer);
                     $('#ModalEdit #color').val(event.color);
                     $('#ModalEdit #start').val(event.start);
                     $('#ModalEdit #end').val(event.end);
                     $('#ModalEdit').modal('show');
                   });
                 },

                 eventResize: function (event, dayDelta, minuteDelta, revertFunc)
                 {
                   edit(event);
                 },
                 events: [
<?php
foreach ($events as $event):

  $start = explode(" ", $event['start']);
  $end = explode(" ", $event['end']);
  if ($start[1] == '00:00:00')
  {
    $start = $start[0];
  } else
  {
    $start = $event['start'];
  }
  if ($end[1] == '00:00:00')
  {
    $end = $end[0];
  } else
  {
    $end = $event['end'];
  }
  ?>
                     {
                       id: '<?php echo $event['id']; ?>',
                       title: '<?php echo $event['title']; ?>',
                       venue: '<?php echo $event['venue']; ?>',
                       lecturer: '<?php echo $event['lecturer']; ?>',
                       start: '<?php echo $start; ?>',
                       end: '<?php echo $end; ?>',
                       color: '<?php echo $event['color']; ?>',
                     },
<?php endforeach; ?>
                 ]
               });

               function edit(event) {
                 start = event.start.format('YYYY-MM-DD HH:mm:ss');
                 if (event.end) {
                   end = event.end.format('YYYY-MM-DD HH:mm:ss');
                 } else {
                   end = start;
                 }

                 id = event.id;

                 Event = [];
                 Event[0] = id;
                 Event[1] = start;
                 Event[2] = end;

                 $.ajax({
                   url: 'editEventDate.php',
                   type: "POST",
                   data: {Event: Event},
                   success: function (rep) {
                     if (rep == 'OK') {
                       alert('Saved');
                     } else {
                       alert('Could not be saved. try again.');
                     }
                   }
                 });
               }
             });
</script>
<div class="background"></div>
</body>

</html>
