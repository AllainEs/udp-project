<?php
		 
	require_once 'bdd.php';
	
	if (isset($_REQUEST['id'])) {
			
		$id = intval($_REQUEST['id']);
		$query = "SELECT * FROM events WHERE id=:id";
		$stmt = $bdd->prepare( $query );
		$stmt->execute(array(':id'=>$id));
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		extract($row);	
		?>
			
		<div class="table-responsive">
		
		<table class="table table-striped table-bordered">
		    <tr>
			    <th>Event Title</th>
				<td><?php echo $title; ?></td>
			</tr>
			<tr>
				<th>Event Venue</th>
				<td><?php echo $venue; ?></td>
			</tr>
			<tr>
				<th>Lecturers</th>
				<td><?php echo $lecturer; ?></td>
			</tr>
      <tr>
				<th>Categories</th>
				<td><?php echo $categories; ?></td>
			</tr>
			<tr>
				<th>Start Time</th>
				<td><?php echo $start; ?></td>
			</tr>
			<tr>
				<th>End Time</th>
				<td><?php echo $end; ?></td>
			</tr>
		</table>
			
		</div>
			
		<?php				
	}