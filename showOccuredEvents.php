
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Occured Events</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">

  </head>

  <body>

    <div class="container">

      <div class="page-header">
        <h3>Occured/Missed Events</h3>
      </div>

      <div class="row">

        <div class="col-lg-12">

          <table class="table  table-bordered">

            <thead>
              <tr>
                <th>Event Name</th>
                <th>View Details</th>
              </tr>
            </thead>

            <tbody>

              <?php
              require_once 'bdd.php';

              $query = "SELECT * FROM events where start <= NOW()";
              $stmt = $bdd->prepare($query);
              $stmt->execute();
              while ($row = $stmt->fetch(PDO::FETCH_ASSOC))
              {
                ?>
                <tr>
                  <td><?php echo $row['title']; ?></td>
                  <td>
                    <button data-toggle="modal" data-target="#view-modal" data-id="<?php echo $row['id']; ?>" id="getDetails" class="btn btn-sm btn-info"><i class="glyphicon glyphicon-eye-open"></i> View</button>
                  </td>
                </tr>
                <?php
              }
              ?>

            </tbody>
          </table>      

        </div>

      </div>




      <div id="view-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog"> 
          <div class="modal-content"> 

            <div class="modal-header"> 
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
              <h4 class="modal-title">
                <i class="glyphicon glyphicon-user"></i> Event Details
              </h4> 
            </div> 
            <div class="modal-body"> 

              <div id="modal-loader" style="display: none; text-align: center;">
                <img src="ajax-loader.gif">
              </div>

              <!-- content will be load here -->                          
              <div id="dynamic-content"></div>

            </div> 
            <div class="modal-footer"> 
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>  
            </div> 

          </div> 
        </div>
      </div><!-- /.modal -->    

    </div>


    <script src="js/jquery-1.12.4.min.js"></script>
    <script src="js/bootstrap.min.js"></script>


    <script>
       $(document).ready(function () {

         $(document).on('click', '#getDetails', function (e) {

           e.preventDefault();

           var uid = $(this).data('id');   // it will get id of clicked row

           $('#dynamic-content').html(''); // leave it blank before ajax call
           $('#modal-loader').show();      // load ajax loader

           $.ajax({
             url: 'occuredEvents.php',
             type: 'POST',
             data: 'id=' + uid,
             dataType: 'html'
           })
             .done(function (data) {
               console.log(data);
               $('#dynamic-content').html('');
               $('#dynamic-content').html(data); // load response 
               $('#modal-loader').hide();		  // hide ajax loader	
             })
             .fail(function () {
               $('#dynamic-content').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
               $('#modal-loader').hide();
             });

         });

       });

    </script>

  </body>
</html>